module.exports = {

    entry: {
        main: "./src/main/javascript/main.js"
    },
    output: {
        path: "./target/generated-resources/js/",
        filename: "hello-react-es6.js"
    },
    module: {
        loaders: [
            { 
                test: /\.(js|jsx)$/, 
                exclude: /node_modules/,
                loader: "babel-loader" 
            }
        ]
    }
};