import React from 'react';
import {forEach} from 'lodash';

import Hello from './components/Hello.jsx';

AJS.toInit(function() {
    const wrappers = document.querySelectorAll('.hello-react-es6');
    _.forEach(wrappers, wrapper => React.render(<Hello/>, wrapper));
});