import React from 'react';

export default React.createClass({

    render: function() {
        const messages = ['webpack', 'babel', 'es2015'].map(label => 
            (<div className="aui-message">
                <p className="title">
                    <strong>Hello {label}!</strong>
                </p>
            </div>)
        );
        return (<div>{messages}</div>);
    }

});